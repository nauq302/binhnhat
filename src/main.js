import { createApp } from 'vue'
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css";
import 'bootstrap'
import "./assets/css/style.css"
import "./assets/css/style-mob.css"
// import "./assets/css/materialize.css"


const app = createApp(App)

app.mount('#app')

import 'bootstrap/dist/js/bootstrap.js'
